package pe.edu.uni.jnaviot.buttonactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ButtonActivity extends AppCompatActivity {
    Button button_1;
    Button button_2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        button_1 = findViewById(R.id.button1);
        button_2 = findViewById(R.id.button2);

        button_1.setOnClickListener(view -> {
          //button_1.setBackgroundColor(getResources().getColor(R.color.new_color));
            //button_1.setBackgroundColor(Color.BLACK);
            button_1.setVisibility(View.INVISIBLE);
            button_2.setVisibility(View.VISIBLE);
        });

    }
}