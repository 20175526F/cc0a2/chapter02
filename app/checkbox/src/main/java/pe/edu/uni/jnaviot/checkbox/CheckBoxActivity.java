package pe.edu.uni.jnaviot.checkbox;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class CheckBoxActivity extends AppCompatActivity {
    TextView textview;
    CheckBox checkbox1;
    CheckBox checkbox2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        textview = findViewById(R.id.text_view);
        checkbox1  = findViewById(R.id.check_box_female);
        checkbox2 = findViewById(R.id.check_box_male);

        checkbox1.setOnClickListener(view -> {
            if(checkbox1.isChecked()){
                textview.setText(R.string.check_female);
            } else{
                textview.setText(R.string.text_view);
            }
        });

        checkbox2.setOnClickListener(view -> {
            if(checkbox2.isChecked()){
                textview.setText(R.string.check_male);
            } else{
                textview.setText(R.string.text_view);
            }
        });

    }
}