package pe.edu.uni.jnaviot.edittext;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditTextActivity extends AppCompatActivity {
    EditText edit_text_1;
    Button button1;
    TextView text_view_1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);

        edit_text_1 = findViewById(R.id.edit_text_1);
        button1 = findViewById(R.id.button_1);
        text_view_1 = findViewById(R.id.text_view_1);

        button1.setOnClickListener(view -> text_view_1.setText(edit_text_1.getText()));



    }
}