package pe.edu.uni.jnaviot.game;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    TextView texViewLastAttempt, texViewRemainAttempts, texViewHint;
    EditText editTextGuessNumber;
    Button buttonFindNumber;
    boolean twoDigits, threeDigits, fourDigits;

    //genera numero aleatorio:
    int randomNumber;
    Random r = new Random();
    int remainingAttempts = 10;

    ArrayList<Integer> guessList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        texViewLastAttempt = findViewById(R.id.text_view_last_attempt);
        texViewRemainAttempts = findViewById(R.id.text_view_remain_attempts);
        texViewHint = findViewById(R.id.text_view_hint);
        editTextGuessNumber = findViewById(R.id.editText_guess_number);
        buttonFindNumber = findViewById(R.id.button_find_number);

        Intent intent = getIntent();
        twoDigits = intent.getBooleanExtra("TWO",false);
        threeDigits = intent.getBooleanExtra("THREE", false);
        fourDigits = intent.getBooleanExtra("FOUR",false);

        if(twoDigits){
            randomNumber = r.nextInt(90)+10;
        }
        if(threeDigits){
            randomNumber = r.nextInt(900)+100;
        }
        if(fourDigits){
            randomNumber = r.nextInt(9000)+1000;
        }
        buttonFindNumber.setOnClickListener(view -> {
            String sGuess = editTextGuessNumber.getText().toString();
            if(sGuess.equals("")){
                Toast.makeText(GameActivity.this,"HH", Toast.LENGTH_LONG ).show();
            }else{
                int iGuess = Integer.parseInt(sGuess);
                guessList.add(iGuess);
                remainingAttempts--;
                texViewLastAttempt.setVisibility(View.VISIBLE);
                texViewRemainAttempts.setVisibility(View.VISIBLE);
                texViewHint.setVisibility(View.VISIBLE);

                Resources res = getResources() ;
                texViewLastAttempt.setText(String.format(res.getString(R.string.text_view_last_attempt),sGuess));
                texViewRemainAttempts.setText(String.format(res.getString(R.string.text_view_remain_attempt), remainingAttempts));

                //win
                if(randomNumber == iGuess){
                    AlertDialog.Builder builder  = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(false);
                    builder.setMessage(String.format(res.getString(R.string.game_msg_win),randomNumber,
                            (10-remainingAttempts), guessList.toString() ));
                    builder.setPositiveButton("Si", (dialogInterface, i) -> {
                        Intent intent1 = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent1);
                        finish();
                    });
                    builder.setNegativeButton("No", (dialogInterface, i) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    });
                    builder.create().show();
                }
                if(randomNumber < iGuess){
                    texViewHint.setText(R.string.text_hint_decrease);
                }
                if(randomNumber > iGuess){
                    texViewHint.setText(R.string.text_hint_increase);
                }
                //lose
                if(remainingAttempts == 0){
                    AlertDialog.Builder builder  = new AlertDialog.Builder(GameActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(false);
                    builder.setMessage(String.format(res.getString(R.string.game_msg_lose),randomNumber,
                            guessList.toString() ));
                    builder.setPositiveButton("Si", (dialogInterface, i) -> {
                        Intent intent1 = new Intent(GameActivity.this, MainActivity.class);
                        startActivity(intent1);
                        finish();
                    });
                    builder.setNegativeButton("No", (dialogInterface, i) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    });
                    builder.create().show();
                }
                editTextGuessNumber.setText("");

            }

        });

    }
}