package pe.edu.uni.jnaviot.game;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    RadioButton radioButton1, radioButton2, radioButton3;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        radioButton3 = findViewById(R.id.radio_button_3);
        button = findViewById(R.id.button_1);

        Intent intent = new Intent(MainActivity.this, GameActivity.class);

        button.setOnClickListener(view -> {
            if(radioButton1.isChecked()){
                intent.putExtra("TWO",true); // boolean
            }
            else if(radioButton2.isChecked()){
                intent.putExtra("THREE",true);
            }
            else if(radioButton3.isChecked()){
                intent.putExtra("FOUR",true);
            }
            else{
                Snackbar.make(view, R.string.snackbar_msg, Snackbar.LENGTH_LONG).show();
                return;
            }
            startActivity(intent);

        });


    }
}