package pe.edu.uni.jnaviot.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {

    GridView gridView;
    List<String> names = new ArrayList<>();
    List<Integer> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GripAdapter adapter = new GripAdapter(this,names,images);

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener((adapterView, view, i, l) -> Toast.makeText(this,names.get(i),Toast.LENGTH_SHORT).show());
    }

    private void fillArray(){
        names.add("Calculadora");
        names.add("Lotus1");
        names.add("Lotus2");
        names.add("Rueda");

        images.add(R.drawable.calculadora);
        images.add(R.drawable.lotus1);
        images.add(R.drawable.lotus2);
        images.add(R.drawable.rueda);
    }

}