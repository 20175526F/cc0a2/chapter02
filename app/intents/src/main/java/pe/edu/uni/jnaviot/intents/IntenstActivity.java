package pe.edu.uni.jnaviot.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IntenstActivity extends AppCompatActivity {

    Button button;
    EditText editText, editNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intenst);

        button = findViewById(R.id.button_1);
        editText = findViewById(R.id.edit_text_1);
        editNumber = findViewById(R.id.edit_text_2);

        button.setOnClickListener(view -> {
            String sText = editText.getText().toString();
            String sNumber = editNumber.getText().toString();
            Intent intent = new Intent(IntenstActivity.this, SecondActivity.class);

            intent.putExtra("TEXT", sText);
            if(!sNumber.equals("")){
                int number = Integer.parseInt(sNumber);
                intent.putExtra("NUMBER",number);
            }

            startActivity(intent);
            //finish();
        });

    }
}