package pe.edu.uni.jnaviot.messages;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {

    Button button_toast;
    Button button_snack_bar;
    Button button_dialog;
    LinearLayout linear_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        button_toast = findViewById(R.id.button_1);
        button_snack_bar = findViewById(R.id.button_2);
        button_dialog = findViewById(R.id.button_3);
        linear_layout = findViewById(R.id.linear_layout);

        button_toast.setOnClickListener(view -> Toast.makeText(getApplicationContext(),R.string.msg_toast,Toast.LENGTH_LONG).show());

        button_snack_bar.setOnClickListener(view -> Snackbar.make(linear_layout, R.string.msg_snack_bar,Snackbar.LENGTH_INDEFINITE).setAction(R.string.msg_button_snack_bar, view1 -> {

        }).show());

        button_dialog.setOnClickListener(view -> {
                   AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle(R.string.titulo)
                            .setMessage(R.string.borrar)
                            .setNegativeButton(R.string.no, (dialogInterface, i) -> dialogInterface.cancel())
                            .setPositiveButton(R.string.yes, (dialogInterface, i) -> {
                                    //something
                            }).show();
                    alertDialog.create();
                });


    }
}