package pe.edu.uni.jnaviot.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class RadioButtonActivity extends AppCompatActivity {

    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    Button button;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        imageView = findViewById(R.id.image_view1);

        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        radioButton3 = findViewById(R.id.radio_button_3);
        button = findViewById(R.id.button_1);

        button.setOnClickListener(view -> {
            if(radioButton1.isChecked()){
                imageView.setImageResource(R.drawable.lotus1);
            }
            if(radioButton2.isChecked()){
                imageView.setImageResource(R.drawable.lotus2);
            }
            if(radioButton3.isChecked()){
                imageView.setImageResource(R.drawable.lotus1);
            }
        });

    }
}