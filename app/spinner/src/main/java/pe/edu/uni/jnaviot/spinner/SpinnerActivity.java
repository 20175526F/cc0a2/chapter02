package pe.edu.uni.jnaviot.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class SpinnerActivity extends AppCompatActivity {

    ImageView image_view;
    Spinner spinner_logo1;
    // Spinner usa Adaptadores

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        image_view = findViewById(R.id.image_view);
        spinner_logo1 = findViewById(R.id.spinner_1);

        adapter = ArrayAdapter.createFromResource(this, R.array.string_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_logo1.setAdapter(adapter);
        spinner_logo1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    image_view.setImageResource(R.drawable.lotus);
                    return;
                }
                if(i==1){
                    image_view.setImageResource(R.drawable.lotus2);
                    return;
                }
                if(i==2){
                    image_view.setImageResource(R.drawable.lotus);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }
}