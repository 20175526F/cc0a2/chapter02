package pe.edu.uni.jnaviot.togglebutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class ToggleButtonActivity extends AppCompatActivity {
    ImageView imageView;
    ToggleButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button);

        imageView = findViewById(R.id.image_view_1);
        toggleButton = findViewById(R.id.toggle_button);

        toggleButton.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b){
                //imageView.setImageResource(R.drawable.lotus2);
                imageView.setVisibility(View.INVISIBLE);                }
            else{
                //imageView.setImageResource(R.drawable.lotus);
                imageView.setVisibility(View.VISIBLE);
            }
        });

    }
}